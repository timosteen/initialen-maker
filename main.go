package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	name := readNameFromArgs()

	initials := getInitials(name)

	printInitials(initials)
}

// Lees de naam uit de command-line argumenten of een bestand als deze is opgegeven
func readNameFromArgs() string {
	args := os.Args[1:]
	if len(args) == 0 {
		fmt.Println("Geef een naam op als command-line argument of in een tekstbestand.")
		os.Exit(1)
	}

	var name string
	if strings.HasSuffix(args[0], ".txt") {
		file, err := os.ReadFile(args[0])
		if err != nil {
			fmt.Printf("Kan het bestand %s niet lezen: %s", args[0], err)
			os.Exit(1)
		}
		name = strings.TrimSpace(string(file))
	} else {
		name = strings.Join(args, " ")
	}
	return name
}

// Genereer de initialen van een naam
func getInitials(name string) string {
	parts := strings.Split(name, " ")

	initials := ""
	for _, part := range parts {
		// Sla bepaalde woorden over
		if strings.Contains(part, "-") || strings.Contains(part, "van") || strings.Contains(part, "de") || strings.Contains(part, "den") {
			continue
		}
		initials += strings.ToUpper(string(part[0]))
	}

	return initials
}

// Print de initialen
func printInitials(initials string) {
	fmt.Println(initials)
}
Nu heeft de code 3 functies:

readNameFromArgs: leest de naam van de command-line argumenten of een bestand als deze is opgegeven.
getInitials: genereert de initialen van een naam.
printInitials: print de initialen naar de standaard uitvoer.
Door deze functies te gebruiken, wordt de hoofdfunctie (main) veel e



